//first NodeJS application 
 
const express = require('express') 
const app = express() 
const port = 3000 
 
app.get('/', (req, res) => { 
  res.send('Hello Virtual Machine!') 
}) 
 
app.listen(port, () => { 
  console.log(`Express Application  listening at port 3000`) 
}) 
 

/*
Program breakdown  
Line 2: Link the application to the express framework 
(this is a library that you will need to install 
inside the project). 
Line 3 Create an instance of an express restful service. 
Line 4 Define a port number for the service to listen on. 
Line 5,6,7 Use the method get to create a hook on the server 
for get requests to the / (aka root) directory resource 
(first parameter of the get method). Also pass to get the 
method to be called when the resource is accessed (req,res) => {}. 
Inside the function we access the res object to 
.send the the text ‘hello virtual machine’. 
Line 8,9,10,11 use the listen method to bind to the port, 
specified as the first argument (3000). 
For the second argument we pass in a method that calls 
console.log() to print some status to 
output of the terminal. 
*/